address 0x14A45D130a7da1a45Ab824BA26921145 {
module NFT {
    use 0x1::Signer;
    use 0x1::Vector;

    struct NFT has key, store { name: vector<u8> }

    struct Uniq has key, store {
        data: vector<vector<u8>>
    }

    public fun initialize(account: &signer) {
        let addr = Signer::address_of(account);
        if (!exists<Uniq>(addr)){
            move_to(account, Uniq {data: Vector::empty<vector<u8>>()});
        };
    }

    public fun create(_account: &signer, registe_center: address, name: vector<u8>): NFT acquires Uniq {
        assert(exists<Uniq>(registe_center), 10000);
        let exist = Vector::contains<vector<u8>>(&borrow_global<Uniq>(registe_center).data, &name);
        assert(!exist, 10001);
        let id_list = borrow_global_mut<Uniq>(registe_center);
        Vector::push_back<vector<u8>>(&mut id_list.data, copy name);
        NFT { name }
    }

    public fun remove(registe_center: address, nft: NFT) acquires Uniq {
        assert(exists<Uniq>(registe_center), 10002);
        let NFT { name } = nft;
        let (exist, index) = Vector::index_of<vector<u8>>(&borrow_global<Uniq>(registe_center).data, &name);
        assert(!exist, 10003);
        let id_list = borrow_global_mut<Uniq>(registe_center);
        Vector::remove<vector<u8>>(&mut id_list.data, index);
    }
}
}
